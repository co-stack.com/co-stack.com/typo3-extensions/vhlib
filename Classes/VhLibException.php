<?php

declare(strict_types=1);

namespace CoStack\VhLib;

use Exception;

abstract class VhLibException extends Exception
{
}
