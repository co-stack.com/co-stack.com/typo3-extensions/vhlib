<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers\Backtracking;

use CoStack\VhLib\ViewHelpers\Backtracking\Exception\InvalidAttemptIdentifierException;
use CoStack\VhLib\ViewHelpers\Backtracking\Exception\NoAttemptInContextException;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperVariableContainer;

use function array_pop;
use function in_array;

abstract class AbstractAttemptControlViewHelper extends AbstractViewHelper
{
    /**
     * @codeCoverageIgnore
     */
    public function initializeArguments(): void
    {
        $this->registerArgument(
            'attempt',
            'string',
            'Identifier of the attempt to accept or blank to accept the current attempt.',
        );
    }

    protected static function getAttemptIdentifier(
        array $arguments,
        ViewHelperVariableContainer $viewHelperVariableContainer,
    ): string {
        $stack = $viewHelperVariableContainer->get(AttemptViewHelper::class, 'stack', []);

        if (empty($stack)) {
            throw new NoAttemptInContextException();
        }

        if (isset($arguments['attempt'])) {
            $identifier = $arguments['attempt'];
            if (!in_array($identifier, $stack, true)) {
                throw new InvalidAttemptIdentifierException($identifier);
            }
            return $identifier;
        }

        return array_pop($stack);
    }
}
