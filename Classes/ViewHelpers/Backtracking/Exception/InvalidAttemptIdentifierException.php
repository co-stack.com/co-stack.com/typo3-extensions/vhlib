<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers\Backtracking\Exception;

use CoStack\VhLib\VhLibException;
use JetBrains\PhpStorm\Pure;

use function sprintf;

/**
 * @codeCoverageIgnore
 */
class InvalidAttemptIdentifierException extends VhLibException
{
    private const MESSAGE = 'The attempt identifier %s does not exist on the current attempt stack';
    public const CODE = 1705950996;

    #[Pure]
    public function __construct(public readonly string $attemptIdentifier)
    {
        parent::__construct(sprintf(self::MESSAGE, $attemptIdentifier), self::CODE);
    }
}
