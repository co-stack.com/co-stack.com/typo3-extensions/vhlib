<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers\Backtracking\Exception;

use CoStack\VhLib\VhLibException;
use JetBrains\PhpStorm\Pure;

/**
 * @codeCoverageIgnore
 */
class CancelAttemptException extends VhLibException
{
    private const MESSAGE = 'You should never see this. This exception is for backtracking purposes only.';
    public const CODE = 1705951071;

    #[Pure]
    public function __construct(public readonly string $attemptIdentifier)
    {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}
