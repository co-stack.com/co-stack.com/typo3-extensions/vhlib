<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers\Backtracking\Exception;

use CoStack\VhLib\VhLibException;
use JetBrains\PhpStorm\Pure;

/**
 * @codeCoverageIgnore
 */
class NoAttemptInContextException extends VhLibException
{
    private const MESSAGE = 'Can not accept or cancel an attempt, because no attempt was started.';
    public const CODE = 1705952074;

    #[Pure]
    public function __construct()
    {
        parent::__construct(self::MESSAGE, self::CODE);
    }
}
