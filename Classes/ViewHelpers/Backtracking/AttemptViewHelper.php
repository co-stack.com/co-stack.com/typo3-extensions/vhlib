<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers\Backtracking;

use Closure;
use CoStack\VhLib\ViewHelpers\Backtracking\Exception\CancelAttemptException;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class AttemptViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    private const IDENTIFIER_ARGUMENT_DESCRIPTION = <<<TXT
The identifier of the attempt. This identifier is used to accept or cancel the attempt.
If the identifier is not provided, any cancellation or acceptance will target the current attempt. 
If you want to target an attempt which is not on the top of the rendering stack, you need an identifier.
TXT;

    public function initializeArguments(): void
    {
        $this->registerArgument('identifier', 'string', self::IDENTIFIER_ARGUMENT_DESCRIPTION);
    }

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext,
    ) {
        $viewHelperVariableContainer = $renderingContext->getViewHelperVariableContainer();

        // Get the provided attempt identifier or create a random one.
        $identifier = $arguments['identifier'] ?? hash('sha1', random_bytes(16));

        // Put the own identity on top of the attempts-stack
        $identifierStack = $originalIdentifierStack = $viewHelperVariableContainer->get(self::class, 'stack', []);
        $identifierStack[] = $identifier;
        $viewHelperVariableContainer->add(self::class, 'stack', $identifierStack);

        // Register the variable to identify if the attempt was accepted
        $viewHelperVariableContainer->add(self::class, 'accepted-' . $identifier, false);

        // Render the children
        $content = '';
        try {
            $content = $renderChildrenClosure();
        } catch (CancelAttemptException $cancelAttemptException) {
            // Bubble up if another attempt than the current one was cancelled.
            if ($cancelAttemptException->attemptIdentifier !== $identifier) {
                throw $cancelAttemptException;
            }
        } finally {
            // Reset the identifier stack to set the previous attempt scope after rendering the children.
            $viewHelperVariableContainer->add(self::class, 'stack', $originalIdentifierStack);
        }

        // Check if the attempt was accepted. Do not return anything if it was not accepted.
        $accepted = $viewHelperVariableContainer->get(self::class, 'accepted-' . $identifier);
        if (!$accepted) {
            $content = '';
        }

        return $content;
    }
}
