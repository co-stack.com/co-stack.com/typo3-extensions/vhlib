<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers\Backtracking;

use Closure;
use CoStack\VhLib\ViewHelpers\Backtracking\Exception\CancelAttemptException;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class CancelViewHelper extends AbstractAttemptControlViewHelper
{
    use CompileWithRenderStatic;

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext,
    ): never {
        $viewHelperVariableContainer = $renderingContext->getViewHelperVariableContainer();
        $identifier = self::getAttemptIdentifier($arguments, $viewHelperVariableContainer);
        throw new CancelAttemptException($identifier);
    }
}
