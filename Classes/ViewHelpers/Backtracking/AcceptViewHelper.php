<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers\Backtracking;

use Closure;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class AcceptViewHelper extends AbstractAttemptControlViewHelper
{
    use CompileWithRenderStatic;

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext,
    ): void {
        $viewHelperVariableContainer = $renderingContext->getViewHelperVariableContainer();
        $identifier = self::getAttemptIdentifier($arguments, $viewHelperVariableContainer);
        $viewHelperVariableContainer->add(AttemptViewHelper::class, 'accepted-' . $identifier, true);
    }
}
