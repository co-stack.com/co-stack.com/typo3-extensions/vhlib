<?php

declare(strict_types=1);

namespace CoStack\VhLib\ViewHelpers;

use Closure;
use TYPO3Fluid\Fluid\Core\Compiler\TemplateCompiler;
use TYPO3Fluid\Fluid\Core\Parser\SyntaxTree\ViewHelperNode;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Examples:
 *  Normal:
 *      <vhlib:implode array="{array}" separator=":" />
 *  Inline:
 *      {vhlib:implode(array: array, separator:':')}
 *  Inline chaining:
 *      {array -> vhlib:implode(separator:';')}}
 */
class ImplodeViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments(): void
    {
        $this->registerArgument('array', 'array', 'The array to implode. Can be provided as child.');
        $this->registerArgument('separator', 'string', 'The string to glue the array pieces together.', false, ',');
    }

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext,
    ) {
        return implode($arguments['separator'], $arguments['array'] ?? $renderChildrenClosure());
    }

    public function compile(
        $argumentsName,
        $closureName,
        &$initializationPhpCode,
        ViewHelperNode $node,
        TemplateCompiler $compiler,
    ): string {
        return 'implode(' . $argumentsName . '[\'separator\'], ' . $argumentsName . '[\'array\'] ?? ' . $closureName . '())';
    }
}
