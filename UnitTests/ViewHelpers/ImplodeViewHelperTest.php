<?php

declare(strict_types=1);

namespace CoStack\VhLib\UnitTests\ViewHelpers;

use CoStack\VhLib\ViewHelpers\ImplodeViewHelper;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\Compiler\TemplateCompiler;
use TYPO3Fluid\Fluid\Core\Parser\SyntaxTree\ViewHelperNode;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContext;

#[CoversClass(ImplodeViewHelper::class)]
class ImplodeViewHelperTest extends TestCase
{
    public function testExecution(): void
    {
        $arguments = [
            'separator' => ';',
            'array' => ['foo', 'bar', 'baz'],
        ];
        $renderChildrenClosure = static fn(): array => [];
        $renderingContext = $this->createMock(RenderingContext::class);
        $actual = ImplodeViewHelper::renderStatic($arguments, $renderChildrenClosure, $renderingContext);
        self::assertSame('foo;bar;baz', $actual);
    }

    public function testCompilation(): void
    {
        $initializationPhpCode = '';
        $node = $this->createMock(ViewHelperNode::class);
        $compiler = $this->createMock(TemplateCompiler::class);

        $implodeViewHelper = new ImplodeViewHelper();
        $code = $implodeViewHelper->compile('$argNameFoo', '$closureNameBar', $initializationPhpCode, $node, $compiler);
        self::assertSame('implode($argNameFoo[\'separator\'], $argNameFoo[\'array\'] ?? $closureNameBar())', $code);

        /**
         * @noinspection PhpArrayWriteIsNotUsedInspection
         * @noinspection PhpUnusedLocalVariableInspection
         */
        $argNameFoo = [
            'separator' => ';',
            'array' => ['foo', 'bar', 'baz'],
        ];
        $result = eval('return ' . $code . ';');
        self::assertSame('foo;bar;baz', $result);
    }
}
