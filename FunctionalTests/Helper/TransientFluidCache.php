<?php

declare(strict_types=1);

namespace CoStack\VhLib\FunctionalTests\Helper;

use TYPO3Fluid\Fluid\Core\Cache\FluidCacheInterface;

use function array_keys;
use function CoStack\Lib\mkdir_deep;
use function escapeshellarg;
use function exec;
use function file_put_contents;
use function unlink;

class TransientFluidCache implements FluidCacheInterface
{
    protected array $code = [];

    public function get($name)
    {
        require $this->code[$name];
        return true;
    }

    public function set($name, $value)
    {
        mkdir_deep(__DIR__ . '/../../Build/var/cache/test/fluid_template_code/');
        $cacheName = __DIR__ . '/../../Build/var/cache/test/fluid_template_code/' . $name . '.php';
        file_put_contents($cacheName, $value);
        $this->code[$name] = $cacheName;
    }

    public function flush($name = null)
    {
        foreach ((array)($name ?? array_keys($this->code)) as $key) {
            if (isset($this->code[$key])) {
                unlink($this->code[$key]);
                unset($this->code[$key]);
            }
        }
    }

    public function getCacheWarmer()
    {
    }

    public function __destruct()
    {
        exec(
            'rm '
            . escapeshellarg('-rf') . ' '
            . escapeshellarg(__DIR__ . '/../../Build/var/cache/test/'),
        );
    }
}
