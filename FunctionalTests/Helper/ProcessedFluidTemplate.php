<?php

declare(strict_types=1);

namespace CoStack\VhLib\FunctionalTests\Helper;

use TYPO3Fluid\Fluid\Core\Parser\ParsedTemplateInterface;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContext;

class ProcessedFluidTemplate
{
    public function __construct(
        protected readonly RenderingContext $renderingContext,
        protected readonly ParsedTemplateInterface $template,
    ) {
    }

    public function setVariables(array $variables): void
    {
        $variableProvider = $this->renderingContext->getVariableProvider();
        foreach ($variables as $name => $value) {
            $variableProvider->add($name, $value);
        }
    }

    public function render(array $variables = []): string
    {
        $this->setVariables($variables);
        return $this->template->render($this->renderingContext);
    }
}
