<?php

declare(strict_types=1);

namespace CoStack\VhLib\FunctionalTests;

use CoStack\VhLib\FunctionalTests\Helper\ProcessedFluidTemplate;
use CoStack\VhLib\FunctionalTests\Helper\TransientFluidCache;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\Parser\TemplateParser;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContext;

use function bin2hex;
use function random_bytes;

abstract class FunctionalViewHelperTestCase extends TestCase
{
    protected function parseFluidTemplate(string $templateCode): ProcessedFluidTemplate
    {
        $renderingContext = new RenderingContext();
        $renderingContext->getViewHelperResolver()->addNamespace('vhlib', 'CoStack\\VhLib\\ViewHelpers\\');
        $templateCompiler = new TemplateParser();
        $templateCompiler->setRenderingContext($renderingContext);
        $parsingState = $templateCompiler->parse($templateCode);
        return new ProcessedFluidTemplate($renderingContext, $parsingState);
    }

    protected function compileFluidTemplate(string $templateCode): ProcessedFluidTemplate
    {
        $renderingContext = new RenderingContext();
        $renderingContext->getViewHelperResolver()->addNamespace('vhlib', 'CoStack\\VhLib\\ViewHelpers\\');
        $renderingContext->setCache(new TransientFluidCache());
        $templateParser = new TemplateParser();
        $templateParser->setRenderingContext($renderingContext);
        $parsingState = $templateParser->parse($templateCode);
        $templateCompiler = $renderingContext->getTemplateCompiler();
        $identifier = 'Test' . bin2hex(random_bytes(16));
        $templateCompiler->store($identifier, $parsingState);
        $parsedTemplate = $templateCompiler->get($identifier);
        return new ProcessedFluidTemplate($renderingContext, $parsedTemplate);
    }
}
