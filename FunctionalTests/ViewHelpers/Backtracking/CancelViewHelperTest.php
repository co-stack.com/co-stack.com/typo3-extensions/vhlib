<?php

declare(strict_types=1);

namespace CoStack\VhLib\FunctionalTests\ViewHelpers\Backtracking;

use CoStack\VhLib\FunctionalTests\FunctionalViewHelperTestCase;
use CoStack\VhLib\ViewHelpers\Backtracking\AbstractAttemptControlViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\AttemptViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\CancelViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\Exception\InvalidAttemptIdentifierException;
use CoStack\VhLib\ViewHelpers\Backtracking\Exception\NoAttemptInContextException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;

#[CoversClass(CancelViewHelper::class)]
#[UsesClass(AbstractAttemptControlViewHelper::class)]
#[UsesClass(AttemptViewHelper::class)]
class CancelViewHelperTest extends FunctionalViewHelperTestCase
{
    public function testAnonymousCancelThrowsExceptionIfNoAttemptWasStarted(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.cancel />
HTML,
        );

        self::expectException(NoAttemptInContextException::class);
        $template->render();
    }

    public function testNamedCancelThrowsExceptionIfNoAttemptWasStarted(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.cancel attempt="foo" />
HTML,
        );

        self::expectException(NoAttemptInContextException::class);
        $template->render();
    }

    public function testNamedCancelThrowsExceptionIfNamedAttemptDoesNotExist(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    <vhlib:backtracking.cancel attempt="bar" />
</vhlib:backtracking.attempt>
HTML,
        );

        self::expectException(InvalidAttemptIdentifierException::class);
        $template->render();
    }
}
