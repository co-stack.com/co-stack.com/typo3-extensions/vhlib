<?php

declare(strict_types=1);

namespace CoStack\VhLib\FunctionalTests\ViewHelpers\Backtracking;

use CoStack\VhLib\FunctionalTests\FunctionalViewHelperTestCase;
use CoStack\VhLib\ViewHelpers\Backtracking\AbstractAttemptControlViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\AcceptViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\AttemptViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\CancelViewHelper;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;

use function trim;

#[CoversClass(AttemptViewHelper::class)]
#[UsesClass(AbstractAttemptControlViewHelper::class)]
#[UsesClass(AcceptViewHelper::class)]
#[UsesClass(CancelViewHelper::class)]
class AttemptViewHelperTest extends FunctionalViewHelperTestCase
{
    public function testAnonymousAttemptWithoutAcceptanceRendersNothing(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt>
    Foo
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('', $actual);
    }

    public function testAnonymousAttemptWithAcceptanceRendersContents(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt>
    Foo
    <vhlib:backtracking.accept />
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('Foo', trim($actual));
    }

    public function testAnonymousAttemptCanBeCancelled(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt>
    Foo
    <vhlib:backtracking.cancel />
    <vhlib:backtracking.accept />
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('', $actual);
    }

    public function testNamedAttemptWithoutAcceptanceRendersNothing(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    Foo
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('', $actual);
    }

    public function testNamedAttemptWithAnonymousAcceptanceRendersContents(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    Foo
    <vhlib:backtracking.accept />
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('Foo', trim($actual));
    }

    public function testNamedAttemptWithNamedAcceptanceRendersContents(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    Foo
    <vhlib:backtracking.accept attempt="foo" />
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('Foo', trim($actual));
    }

    public function testNamedAttemptCanBeAnonymouslyCancelled(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    Foo
    <vhlib:backtracking.cancel />
    <vhlib:backtracking.accept />
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('', $actual);
    }

    public function testNamedAttemptCanBeNamedCancelled(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    Foo
    <vhlib:backtracking.cancel attempt="foo" />
    <vhlib:backtracking.accept />
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('', $actual);
    }

    public function testNamedParentAttemptCanBeCancelled(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    Foo
    <vhlib:backtracking.attempt identifier="bar">
        Bar
        <vhlib:backtracking.accept />
        <vhlib:backtracking.cancel attempt="foo" />
    </vhlib:backtracking.attempt>
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('', $actual);
    }

    public function testNestedAttemptCanBeCancelled(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    Foo
    <vhlib:backtracking.attempt identifier="bar">
        Bar
        <vhlib:backtracking.accept />
        <vhlib:backtracking.cancel attempt="bar" />
    </vhlib:backtracking.attempt>
    <vhlib:backtracking.accept />
</vhlib:backtracking.attempt>
HTML,
        );
        $actual = $template->render();
        self::assertSame('Foo', trim($actual));
    }
}
