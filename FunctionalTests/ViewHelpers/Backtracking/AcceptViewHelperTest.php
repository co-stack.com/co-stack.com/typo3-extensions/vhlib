<?php

declare(strict_types=1);

namespace CoStack\VhLib\FunctionalTests\ViewHelpers\Backtracking;

use CoStack\VhLib\FunctionalTests\FunctionalViewHelperTestCase;
use CoStack\VhLib\ViewHelpers\Backtracking\AbstractAttemptControlViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\AcceptViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\AttemptViewHelper;
use CoStack\VhLib\ViewHelpers\Backtracking\Exception\InvalidAttemptIdentifierException;
use CoStack\VhLib\ViewHelpers\Backtracking\Exception\NoAttemptInContextException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;

#[CoversClass(AcceptViewHelper::class)]
#[UsesClass(AbstractAttemptControlViewHelper::class)]
#[UsesClass(AttemptViewHelper::class)]
class AcceptViewHelperTest extends FunctionalViewHelperTestCase
{
    public function testAnonymousAcceptThrowsExceptionIfNoAttemptWasStarted(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.accept />
HTML,
        );

        self::expectException(NoAttemptInContextException::class);
        $template->render();
    }

    public function testNamedAcceptThrowsExceptionIfNoAttemptWasStarted(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.accept attempt="foo" />
HTML,
        );

        self::expectException(NoAttemptInContextException::class);
        $template->render();
    }

    public function testNamedAcceptThrowsExceptionIfNamedAttemptDoesNotExist(): void
    {
        $template = $this->parseFluidTemplate(
            <<<HTML
<vhlib:backtracking.attempt identifier="foo">
    <vhlib:backtracking.accept attempt="bar" />
</vhlib:backtracking.attempt>
HTML,
        );

        self::expectException(InvalidAttemptIdentifierException::class);
        $template->render();
    }
}
