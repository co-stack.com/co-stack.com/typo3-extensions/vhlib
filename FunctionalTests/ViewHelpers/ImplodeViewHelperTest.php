<?php

declare(strict_types=1);

namespace CoStack\VhLib\FunctionalTests\ViewHelpers;

use CoStack\VhLib\FunctionalTests\FunctionalViewHelperTestCase;
use CoStack\VhLib\ViewHelpers\ImplodeViewHelper;
use PHPUnit\Framework\Attributes\CoversClass;

#[CoversClass(ImplodeViewHelper::class)]
class ImplodeViewHelperTest extends FunctionalViewHelperTestCase
{
    public function testParsedTagViewHelper(): void
    {
        $template = $this->parseFluidTemplate('<vhlib:implode array="{0: \'foo\', 1: \'bar\'}" separator=";" />');
        $actual = $template->render();
        self::assertSame('foo;bar', $actual);
    }

    public function testCompiledTagViewHelper(): void
    {
        $template = $this->compileFluidTemplate(
            '<vhlib:implode array="{0: \'foo\', 1: \'bar\'}" separator=";" />',
        );

        $actual = $template->render();
        self::assertSame('foo;bar', $actual);
    }

    public function testCompiledInlineViewHelper(): void
    {
        $template = $this->compileFluidTemplate(
            '{vhlib:implode(array:{0: \'foo\', 1: \'bar\'}, separator:\';\')}',
        );

        $actual = $template->render();
        self::assertSame('foo;bar', $actual);
    }

    public function testCompiledInlineChainedViewHelper(): void
    {
        $template = $this->compileFluidTemplate(
            '{array -> vhlib:implode(separator:\';\')}',
        );

        $actual = $template->render(['array' => ['foo', 'bar']]);
        self::assertSame('foo;bar', $actual);
    }
}
